;; -*- coding: utf-8; lexical-binding: t -*-
;;; init.el --- Eeli's Emacs config
;;
;; Copyright (C) 2009-2021 Eeli Reilin
;;
;;; Commentary:
;; Personal Emacs configuration
;;

;; TODO: This is a hack to make Emacs work
(defun load-history-filename-element (file-regexp)
  "Get the first elt of `load-history' whose car matches FILE-REGEXP.
Return nil if there isn't one."
  (let* ((loads load-history)
	 (load-elt (and loads (car loads))))
    (save-match-data
      (while (and loads
		  (or (null (car load-elt))
		      (not (and (stringp (car load-elt))
                                (string-match file-regexp (car load-elt))))))
	(setq loads (cdr loads)
	      load-elt (and loads (car loads)))))
    load-elt))

(defmacro with-system (type &rest body)
  "Evaluate BODY if `system-type' equals TYPE."
  (declare (indent defun))
  `(when (eq system-type ',type)
     ,@body))

(defmacro with-default-directory (dir &rest body)
  "Evaluate `BODY' in the given directory `DIR'.

Restores the original `default-directory' when `BODY' has been evaluated."
  (declare (indent defun))
  `(let ((defdir default-directory))
     (setq default-directory ,dir)
     ,@body
     (setq default-directory defdir)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; startup settings

(add-to-list 'load-path (expand-file-name "vendor/" user-emacs-directory))

;; no startup screen
(setq inhibit-startup-message t)

(defconst user-site-lisp-directory
  (expand-file-name "site-lisp/" user-emacs-directory))

;; save custom variables to their own file
(setq custom-file (concat (file-name-as-directory user-emacs-directory) "custom.el"))
(unless (file-exists-p custom-file)
  (with-temp-buffer (write-file custom-file)))

;; disable scrollbars, toolbar and menubar

(menu-bar-mode -1)
(when (and (eq window-system 'ns)
           (eq system-type 'darwin))
  (menu-bar-mode 1))

(when (display-graphic-p)
  (scroll-bar-mode -1)
  (tool-bar-mode -1))
(setq-default indicate-unused-lines nil)

(defconst font-size 160
  "Current font size.")

(defvar presentation-mode nil)
(defun toggle-presentation-mode ()
  "Toggle between large and small font size."
  (interactive)
  (if presentation-mode
      (text-scale-adjust 0)
    (text-scale-adjust 2))
  (setq presentation-mode (if presentation-mode nil t)))

(global-set-key (kbd "<f12>") #'toggle-presentation-mode)

(setq frame-resize-pixelwise t)

(defun set-font ()
  "Define custom default font."
  (let ((font (find-font (font-spec :name "SF Mono" :weight 'regular)))
        (size font-size))
    (when font
      (setq font-size size)
      (set-frame-font font)
      (set-face-attribute 'bold nil :height size :weight 'semi-bold)
      (set-face-attribute 'default nil :height size :weight 'regular)
      (set-face-attribute 'font-lock-comment-face nil :weight 'ultra-light))))
(set-font)

;; Enable ligatures
(with-system darwin
  (setq mac-auto-operator-composition-mode t))

;; Use camel case word delimiter in minibuffer
(add-hook 'minibuffer-setup-hook 'subword-mode)

(require 'package)
(setq package-enable-at-startup nil)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/"))

(package-initialize)

;; bootstrap `use-package'
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(eval-when-compile
  (require 'use-package))
;(require 'diminish)
(require 'bind-key)

(add-hook 'window-setup-hook 'toggle-frame-maximized)

(add-to-list 'exec-path "/usr/local/bin")

(setq create-lockfiles nil)

;; optimization to prevnt emacs from blocking when finding files in large repos (C-x C-f)
(remove-hook 'file-name-at-point-functions 'ffap-guess-file-name-at-point)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; global functions

(defun point-at-indentation ()
  "Return non-nil if point is at indentation, nil otherwise."
  (= (save-excursion (back-to-indentation) (point)) (point)))

(defun beginning-of-line-or-indentation ()
  "Toggle between beginning of line and point of indentation."
  (interactive)
  (if (point-at-indentation)
      (beginning-of-line)
    (back-to-indentation)))

(defun comment-or-uncomment-region-or-line ()
  "Comment or uncomment the region or the current line."
  (interactive)
  (let (beg end)
    (if (region-active-p)
        (setq beg (region-beginning) end (region-end))
      (setq beg (line-beginning-position) end (line-end-position)))
    (comment-or-uncomment-region beg end)))

(defun clean-trailing-whitespace ()
  "Delete all trailing whitespace from buffer."
  (interactive)
  (save-excursion
    (mark-whole-buffer)
    (delete-trailing-whitespace)))

(defun kill-current-buffer ()
  "Kill the current buffer."
  (interactive)
  (kill-buffer (current-buffer)))

(defun q-to-quit ()
  "Bind q to quit the current window."
  (local-set-key "q" (lambda ()
                       (interactive)
                       (quit-window t))))

(defun make-scratch ()
  "Make and switch to a new scratch buffer if one does not exist."
  (interactive)
  (let ((name "*Scratch*"))
    (switch-to-buffer (or (get-buffer name) name))
    (emacs-lisp-mode)))

(defun disable-word-wrap ()
  "Disable wordwrap for the buffer when called."
  (interactive)
  (setq-local truncate-lines 0))

(defun my-counsel-find-file ()
  "Custom version of `counsel-find-file'.

Same as `counsel-find-file' but sets `default-directory' to a
directory that contains current buffer.

This is a workaround for a hack that keeps flycheck working with typescript."
  (interactive)
  (with-default-directory (if buffer-file-name
                              (file-name-directory buffer-file-name)
                            "~/")
    (counsel-find-file)))

(defalias 'use-package-normalize/:system 'use-package-normalize-test)
(defun use-package-handler/:system (name keyword pred rest state)
  (let ((body (use-package-process-keywords name rest state)))
    `((when (eq system-type ',pred) ,@body))))
(add-to-list 'use-package-keywords :system)

(defun pretty-hydra-project-title ()
  (let ((p (projectile-project-name)))
    (propertize (concat (all-the-icons-octicon "octoface")
                        " "
                        (if (s-equals? p "-") "Projects" p))
                :face 'font-lock-builtin-face)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; global settings

(put 'narrow-to-region 'disabled nil)
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)

;; Disable horizontal splits
(setq split-height-threshold nil
      split-width-threshold 160)

(global-set-key (kbd "M-.") #'xref-find-definitions-other-window)

(setq-default indent-tabs-mode nil)

;; y/n instead of yes/no
;(defalias 'yes-or-no-p 'y-or-n-p)

;; show mark and region
(setq transient-mark-mode t)
;; show line and column numbers on the modeline
(setq line-number-mode t)
(setq column-number-mode t)

;; delete region by writing or by backspace
(delete-selection-mode 1)

;; explicitly show the end of a buffer
(set-default 'indicate-empty-lines t)

;; make sure all backup files only live in one place
(setq backup-directory-alist '(("." . "~/.emacs-backups")))
(setq backup-inhibited t)

;; no autosave
(setq auto-save-default nil)

;; disable line wrapping
(set-default 'truncate-lines t)

;; show matching parens
(show-paren-mode t)

(setq save-interprogram-paste-before-kill t)

;; No beep
(setq-default visual-bell t)

;; no line truncation
(setq truncate-lines t)
(setq truncate-partial-width-windows nil)

;; faster buffer killing
(global-set-key (kbd "M-k") #'kill-current-buffer)

(setq comp-async-report-warnings-errors nil)

;; show current buffer filename as frame title
(setq frame-title-format
      '((:eval (if (buffer-file-name)
                   (abbreviate-file-name (buffer-file-name))
                 "%b"))))

;; Better titlebar look and feel for Mac OS
(with-system darwin
  (add-to-list 'default-frame-alist '(ns-transparent-titlebar . t))
  (add-to-list 'default-frame-alist '(ns-appearance . dark))

  (setq mac-option-modifier 'super
        mac-command-modifier 'meta
        mac-right-option-modifier 'none))

(defun use-internal-keyboard ()
  (interactive)
  (setq mac-option-modifier 'super
        mac-command-modifier 'meta))
(defun use-external-keyboard ()
  (interactive)
  (setq mac-option-modifier 'meta
        mac-command-modifier 'super))

;; Load color theme
(use-package doom-themes
  :ensure t
  :init (progn
          (setq doom-themes-enable-bold t
                doom-themes-enable-italic t)
          (load-theme 'doom-challenger-deep t)))
  
(use-package doom-modeline
      :ensure t
      :hook (after-init . doom-modeline-mode)
      :config (progn
                (setq doom-themes-enable-bold t
                      doom-themes-enable-italic t
                      doom-modeline-icon nil
                      doom-modeline-bar-width 1
                      doom-modeline-icon t
                      doom-modeline-major-mode-icon t
                      doom-modeline-major-mode-color-icon t
                      doom-modeline-buffer-file-name-style 'truncate-upto-project
                      doom-modeline-buffer-state-icon t)
                (doom-themes-visual-bell-config)))

;; hide vertical border between windows
;(set-face-foreground 'vertical-border (face-background 'default))
;; make fringe more subtle
;(set-face-attribute 'fringe nil :background (face-background 'default))

(global-set-key (kbd "C-a") 'beginning-of-line-or-indentation)
(global-set-key (kbd "C-;") 'comment-or-uncomment-region-or-line)

(defun toggle-eshell ()
  (interactive)
  (if (get-buffer-window "*eshell*")
      (quit-window)
    (if-let ((buf (get-buffer "*eshell*")))
        (display-buffer buf)
      (eshell))))

(global-set-key (kbd "<f7>") 'toggle-eshell)

;; Line numbers for programming modes
(add-hook 'prog-mode-hook 'display-line-numbers-mode)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; package settings

(use-package grugru
  :ensure t
  :init (grugru-default-setup)
  :bind (("C-\'" . #'grugru))
  :pin melpa-stable)

(use-package slack
  :ensure t
  :after pretty-hydra
  :commands (slack-start)
  :init (setq slack-buffer-emojify t
              slack-prefer-current-team t
              slack-buffer-create-on-notify t
              slack-enable-global-mode-string t
              lui-fill-type nil
              lui-time-stamp-position 'left)
  :config (slack-register-team
           :name "Firstbeat"
           :default t
           :token "xoxs-212200364210-584070187142-1263257267297-af0473dca0bcd18d9b47a971837134810373fd45b87d83a45f81a1ba1679a16b"
           :subscribed-channels '()
           :full-and-display-names nil)
  :pretty-hydra
  ((:color "#ff5d38" :quit-key "q" :title "Slack")
   ("Channels"
    (("c"   slack-channel-select          "Select channel")
     ("j"   slack-channel-join            "Join")
     ("l"   slack-channel-leave           "Leave")
     ("i"   slack-channel-invite          "Invite"))
    "Direct messages"
    (("u"   slack-user-select             "Select user")
     ("d"   slack-im-select               "Select conversation"))
    "Rooms"
    (("r"   slack-select-rooms            "Select rooms")
     ("R"   slack-select-unread-rooms     "Select unread rooms"))
    "Groups"
    (("g"   slack-group-select            "Select group")
     ("L"   slack-group-leave             "Leave group")
     ("I"   slack-group-invite            "Invite"))))
  :bind (("C-c s" . slack-hydra/body)))

(use-package alert
  :commands (alert)
  :init
  (setq alert-default-style 'notifier))

(use-package parrot
  :ensure t
  :init
  (progn (parrot-mode t)
         (setq parrot-num-rotations 5))
  :hook ((after-save . parrot-start-animation)))

(use-package dired
  :hook ((dired-mode . dired-hide-details-mode)
         (dired-mode . all-the-icons-dired-mode))
  :bind (:map dired-mode-map
              ("<tab>" . dired-hide-subdir))
  :config
  (progn
    (with-system darwin
      (setq insert-directory-program "/usr/local/opt/coreutils/libexec/gnubin/ls"
            dired-listing-switches "-lah --group-directories-first"
            dired-use-ls-dired t))))

(use-package dired-git-info
    :ensure t
    :bind (:map dired-mode-map
                (")" . dired-git-info-mode)))

(use-package esh
  :config (progn
            (add-to-list 'eshell-visual-commands "yarn")

            (defun my-eshell-prompt-function ()
              (format "%s %s %s "
                      (all-the-icons-octicon "file-directory")
                      (propertize (abbreviate-file-name default-directory) 'face `(:foreground "white"))
                      (propertize "λ" 'face `(:foreground "#ff79c6"))))

            (setq eshell-glob-case-insensitive t
                  eshell-cmpl-ignore-case t
                  eshell-history-size 1000000
                  eshell-destroy-buffer-when-process-dies t
                  eshell-hist-ignoredups t
                  eshell-prompt-function #'my-eshell-prompt-function))
  :bind (:map eshell-mode-map
              ("C-r" . counsel-esh-history)
              ("C-c C-c" . eshell-send-eof-to-process)))

(use-package yasnippet
  :ensure t
  :pin melpa-stable
  :init (yas-global-mode t)
  :config (setq yas-snippet-dirs `(,(concat (file-name-as-directory user-emacs-directory)
                                           "snippets"))))

(use-package all-the-icons
  :ensure t)

(use-package all-the-icons-ivy
  :ensure t
  :after ivy
  :config (progn
            (all-the-icons-ivy-setup)
            (setq all-the-icons-ivy-file-commands
                  '(counsel-find-file counsel-file-jump counsel-recentf counsel-projectile-find-file counsel-projectile-find-file-in counsel-projectile-find-dir))))

(use-package swiper
  :ensure t
  :bind (("C-s" . swiper)
         ("C-r" . swiper-backward))
  :pin melpa-stable)

(use-package counsel
  :ensure t
  :pin melpa-stable
  :bind (("M-x" . counsel-M-x)
         ("C-x C-f" . my-counsel-find-file)
         ("C-x b" . counsel-switch-buffer)
         ("M-y" . counsel-yank-pop)))

(use-package counsel-projectile
  :ensure t)

(use-package ivy
  :ensure t
  :pin melpa-stable
  :init (ivy-mode 1)
  :config (progn
            (setq enable-recursive-minibuffers t
                  ivy-height 16
                  ivy-re-builders-alist '((t . ivy--regex-plus)))
            (define-key minibuffer-local-map (kbd "C-r") 'counsel-minibuffer-history)

            (defun ivy-insert-action (x)
              "Insert the relative path to the file into the current buffer."
              (with-ivy-window
                (insert (file-relative-name x buffer-file-name))))

            (ivy-set-actions t '(("I" ivy-insert-action "insert relative path")))))

(use-package pretty-hydra
  :ensure t)

(use-package paren-face
  :ensure t
  :hook ((emacs-lisp-mode . paren-face-mode))
  :config (set-face-attribute 'parenthesis nil :foreground "gray90"))

(use-package exec-path-from-shell
  :ensure t
  :pin melpa-stable
  :init (progn
          (when (memq window-system '(mac ns x))
            (exec-path-from-shell-initialize))))

(use-package shackle
  :ensure t
  :config (progn
            (shackle-mode 1)
            (add-to-list 'shackle-rules '("*Help*" :align below :size 16 :select t))
            (add-to-list 'shackle-rules '("*Messages*" :align below :size 16 :select t))
            (add-to-list 'shackle-rules '("*Backtrace*" :align below :size 16 :select t))
            (add-to-list 'shackle-rules '("*HTTP Response*" :align right :popup t :select t))
            (add-to-list 'shackle-rules '("*tide-documentation*" :align below :size 16 :popup t :select t))
            (add-to-list 'shackle-rules '("*tide-references*" :align below :size 16 :popup t :select t))
            (add-to-list 'shackle-rules '("*[a-z0-9\\-]+-errors*" :regexp t :align below :size 24 :popup t :select t))
            (add-to-list 'shackle-rules '("*Flycheck errors*" :align below :size: 24 :popup t :select t))
            (add-to-list 'shackle-rules '("*grep*" :other t :select t :popup t))
            (add-to-list 'shackle-rules '("*Packages*" :align below :size 24 :select t :popup t))
            (add-to-list 'shackle-rules '("*eshell*" :align below :popup t :size 32 :select t))
            (add-to-list 'shackle-rules '("*sh*" :align below :popup t :size 32 :select t))
            (add-to-list 'shackle-rules '("*sh*" :align below :popup t :size 32 :select t)))
  :pin melpa-stable)

(use-package lsp-mode
  :ensure t
  :commands lsp
  :hook (lsp-mode-hook . lsp-ui-mode)
  :config (progn
            (setq lsp-prefer-flymake :none
                  lsp-enable-snippet nil))
  :pin melpa)
(use-package lsp-ui
  :ensure t
  :bind (("M-RET" . lsp-ui-sideline-apply-code-actions)
         ("M-?" . lsp-ui-peek-find-references))
  :pin melpa)
(use-package company-lsp
  :ensure t
  :commands company-lsp)

(use-package company
  :defer t
  :commands global-company-mode
  :diminish company-mode
  :init (progn
          (add-hook 'after-init-hook 'global-company-mode)
          (global-set-key (kbd "C-.") #'company-complete))
  :config (progn
            (add-to-list 'company-backends 'company-lsp 'company-yasnippet)
            (setq company-minimum-prefix-length 1
                  company-idle-delay 0.5
                  company-tooltip-idle-delay 0.5))
  :pin gnu)

(defun projectile-eshell ()
  "Open eshell in current project's root directory."
  (interactive)
  (let ((default-directory (projectile-project-name)))
    (eshell '(4))))

(use-package projectile
  :ensure t
  :init (projectile-mode t)
  :config (progn
            (setq projectile-project-search-path '("~/Projects/")
                  projectile-indexing-method 'hybrid)
            (add-to-list 'projectile-globally-ignored-files "*.png")
            (add-to-list 'projectile-globally-ignored-files "*.ttf")
            (projectile-discover-projects-in-search-path))
  :pretty-hydra
  ((:color "#ff5d38" :quit-key "q" :title (pretty-hydra-project-title))
   ("Files"
    (("f"   counsel-projectile-find-file        "Find file")
     ("b"   counsel-projectile-switch-to-buffer "Jump to a directory")
     ("r"   projectile-recentf                  "Recent files")
     ("k"   projectile-kill-buffers             "Kill buffers"))
    "Search"
    (("r"   counsel-projectile-rg               "Ripgrep project")
     ("g"   projectile-find-file-in-directory   "Find file in directory"))
    ""
    (("d"   counsel-projectile-find-dir         "Find directory")
     ("s"   projectile-eshell                   "Eshell")
     ("c"   projectile-invalidate-cache         "Clear cache"))
    "Projects"
    (("p"   counsel-projectile-switch-project   "Switch project")
     ("o"   projectile-multi-occur              "Multi occur")
     ("x"   projectile-remove-known-project     "Remove known project")
     ("X"   projectile-cleanup-known-projects   "Cleanup known projects"))))
  :bind (("C-c p" . projectile-hydra/body)))

(use-package smerge
  :pretty-hydra
  ((:color "#ff5d38" :quit-key "q" :title "SMerge")
   ("Move"
    (("n" smerge-next "Next conflict")
     ("p" smerge-prev "Previous conflict"))
    "Resolve"
    (("u" smerge-keep-upper "Keep upper")
     ("l" smerge-keep-lower "Keep lower")
     ("a" smerge-keep-all "Keep all")
     ("b" smerge-keep-base "Revert to base"))
    "Other"
    (("e" smerge-ediff "Invoke ediff")
     ("r" smerge-resolve "Auto resolve"))))
  :bind (("C-c m" . smerge-hydra/body))
  :hook ((smerge-mode . smerge-hydra/body)))

(use-package magit
  :ensure t
  :commands magit-status
  :config (progn
            (setq magit-last-seen-setup-instructions "1.4.0"
                  magit-diff-refine-hunk 'all
                  magit-push-always-verify nil
                  magit-git-executable "/usr/local/bin/git")
            (defadvice magit-status (around magit-fullscreen activate)
              ;; fullscreen magit-status
              (window-configuration-to-register :magit-fullscreen)
              ad-do-it
              (delete-other-windows))
            (defun magit-quit-session ()
              "Restore the previous window configuration and kill the magit buffer"
              (interactive)
              (kill-buffer)
              (jump-to-register :magit-fullscreen))
            (with-eval-after-load "magit"
              (define-key magit-status-mode-map (kbd "q") 'magit-quit-session))
            (add-hook 'magit-process-mode-hook 'goto-address-mode)

            (remove-hook 'magit-status-sections-hook 'magit-insert-tags-header)
            (remove-hook 'magit-status-sections-hook 'magit-insert-unpushed-to-pushremote)
            (remove-hook 'magit-status-sections-hook 'magit-insert-unpulled-from-pushremote)
            (remove-hook 'magit-status-sections-hook 'magit-insert-unpulled-from-upstream)
            (remove-hook 'magit-status-sections-hook 'magit-insert-unpushed-to-upstream-or-recent)
            (remove-hook 'magit-status-headers-hook 'magit-insert-tags-header)
            (remove-hook 'magit-status-headers-hook 'magit-insert-upstream-branch-header)
            (remove-hook 'magit-status-headers-hook ' magit-insert-push-branch-header))
  :bind ("C-c vs" . magit-status)
  :pin melpa-stable)

(use-package recentf
  :init (recentf-mode 1)
  :config (progn (setq recentf-max-menu-items 256
                       recentf-auto-cleanup 'never)
                 (add-to-list 'recentf-exclude ".emacs.d/elpa"))
  :bind ("C-x C-r" . counsel-recentf))

(use-package git-gutter
  :ensure t
  :hook ((markdown . git-gutter-mode)
         (prog . git-gutter-mode)
         (conf . git-gutter-mode)
         (web . git-gutter-mode))
  :config (progn (setq git-gutter:disabled-modes '(org-mode asm-mode image-mode)
                       git-gutter:update-interval 1
                       git-gutter:window-width 1
                       git-gutter:ask-p nil)
                 (if (boundp 'fringe-mode)
                     (fringe-mode))))

(use-package git-gutter-fringe
  :ensure t
  :diminish git-gutter-mode
  :after git-gutter
  :config (progn
            (global-git-gutter-mode 1)
            (setq git-gutter-fr:side 'left-fringe)
            (setq-default fringes-outside-margins t)

            (define-fringe-bitmap 'git-gutter-fr:added [224] nil nil '(center repeated))
            (define-fringe-bitmap 'git-gutter-fr:modified [224] nil nil '(center repeated))
            (define-fringe-bitmap 'git-gutter-fr:deleted [224] nil nil '(center repeated))
            (define-fringe-bitmap 'flycheck-fringe-bitmap-double-arrow [0 60 126 126 60 0] nil nil 'center))
  :pin melpa-stable)

(use-package ace-jump-mode
  :ensure t
  :defer t
  :bind ("C-c j" . ace-jump-mode)
  :pin melpa-stable)

(use-package expand-region
  :ensure t
  :defer t
  :bind ("C-c e" . er/expand-region)
  :pin melpa-stable)

(use-package multiple-cursors
  :ensure t
  :defer t
  :bind (("C-<" . mc/mark-previous-like-this)
         ("C-M-<" . mc/skip-to-previous-like-this)
         ("C->" . mc/mark-next-like-this)
         ("C-M->" . mc/skip-to-next-like-this))
  :pin melpa-stable)

(use-package flycheck
  :after git-gutter-fringe
  :commands flycheck-mode
  :diminish flycheck-mode
  :init (progn
          (defun flycheck-animate-parrot ()
            (interactive)
            (unless flycheck-current-errors
              (parrot-start-animation)))
          (flycheck-add-mode 'javascript-eslint 'typescript-mode)
          (add-hook 'flycheck-mode-hook 'flycheck-rust-setup)
          (add-hook 'flycheck-after-syntax-check-hook 'flycheck-animate-parrot))
  :config (setq flycheck-indication-mode 'left-fringe)
  :pin melpa-stable
  :pretty-hydra
  ((:color "#ff5d38" :quit-key "q" :title "Flycheck commands")
   ("Errors"
    (("n" flycheck-next-error       "Next")
     ("p" flycheck-previous-error   "Previous")
     ("a" flycheck-list-errors      "All")
     ("c" flycheck-clear            "Clear"))
    "Syntax checker"
    (("s" flycheck-select-checker   "Select checker")
     ("d" flycheck-disable-checker  "Disable checker")
     ("?" flycheck-describe-checker "Describe checker"))))
  :bind (("C-c f" . flycheck-hydra/body)))

(use-package whitespace
  :commands whitespace-mode)

(use-package prettify-symbols
  :defer t
  :init (progn
          (defun lisp-prettify-hook ()
            (setq prettify-symbols-alist
                  '(("lambda" . ?λ))))
          (defun typescript-prettify-hook ()
            (setq prettify-symbols-alist
                  '(("=>" . ?→)
                    ("null" . ?∅)
                    ("!==" . ?≠))))
          (defun rust-prettify-hook ()
            (setq prettify-symbols-alist
                  '(("=>" . ?⇒) ("..." . ?…) ("||" . ?∨)
                    ("->" . ?→) ("!" . ?¬) ("&&" . ?∧)))))
  :hook ((lisp-mode . lisp-prettify-hook)
         (rust-mode . rust-prettify-hook)
         (typescript-mode . typescript-prettify-hook)
         (web-mode . typescript-prettify-hook))
  :commands prettify-symbols-mode
  :diminish prettify-symbols-mode)

(use-package indent-guide
  :ensure t
  :pin melpa-stable)

(use-package eldoc
  :commands eldoc-mode
  :diminish eldoc-mode)

(use-package emacs-lisp
  :commands emacs-lisp-mode
  :diminish (emacs-lisp-mode . "λ"))

(use-package lisp-mode
  :config (progn
            (add-hook 'emacs-lisp-mode-hook 'eldoc-mode)
            (add-hook 'lisp-interaction-mode-hook 'eldoc-mode)
            (add-hook 'emacs-lisp-mode-hook 'prettify-symbols-mode)))

(use-package java-mode
  :commands java-mode
  :hook ((java-mode . prettify-symbols-mode)
         (java-mode . indent-guide-mode)))

(use-package ccls
    :ensure t
    :hook ((c-mode c++-mode objc-mode cuda-mode) . (lambda () (require 'ccls) (lsp))))

(use-package js-mode
  :defer t
  :hook ((js-mode . prettier-js-mode)))

(use-package typescript-mode
  :ensure t
  :mode (("\\.ts$" . typescript-mode)
         ("\\.tsx$" . typescript-mode))
  :config (progn
            (flycheck-add-mode 'javascript-eslint 'typescript-mode)
            (setq typescript-indent-level 2)
            (with-eval-after-load "grugru"
              (grugru-define-on-major-mode 'typescript-mode 'symbol '("true" "false"))
              (grugru-define-on-major-mode 'typescript-mode 'word '("enabled" "disabled"))
              (grugru-define-on-major-mode 'typescript-mode 'word '("enable" "disable"))))
  :hook ((typescript-mode . electric-pair-mode)
         (typescript-mode . electric-indent-mode)
         (typescript-mode . tide-mode)
         (typescript-mode . flycheck-mode)
         (typescript-mode . prettier-js-mode)
         (typescript-mode . tree-sitter-hl-mode)))

(use-package tree-sitter :ensure t :after typescript-mode)
(use-package tree-sitter-langs :ensure t :after typescript-mode)

(use-package rust-mode
  :ensure t
  :mode ("\\.rs$" . rust-mode)
  :init (progn
          (use-package cargo :ensure t :pin melpa-stable)
          (use-package flycheck-rust :ensure t))
  :hook ((rust-mode . cargo-minor-mode)
         (rust-mode . indent-guide-mode)
         (rust-mode . electric-pair-mode))
  :config (progn
            (add-to-list 'exec-path "~/.cargo/bin")
            (add-to-list 'exec-path "~/.multirust/toolchains/nightly-x86_64-apple-darwin/bin/")

            (add-to-list 'shackle-rules '("*Cargo Build*" :align below :select t))
            (add-to-list 'shackle-rules '("*Cargo Check*" :align below :select t))

            (setq rust-indent-offset 4
                  fill-column 100
                  rust-format-on-save t))
  :bind (:map rust-mode-map
         ("C-c f" . rust-format-buffer)
         ("<f5>" . cargo-process-build))
  :pin melpa-stable)

(use-package cc-mode
  :init (progn
          (setq c-default-style "stroustrup"
                c-basic-offset 4)))

(use-package tide
  :after (flycheck company)
  :ensure t
  :commands tide-mode
  :init (flycheck-add-mode 'javascript-eslint 'typescript-mode)
  :config (progn
            (defun tide-flycheck-setup ()
              (defun flycheck-eslint-config-exists-p ()
                "Whether there is a valid eslint config for the current buffer."
                (let* ((executable (flycheck-find-checker-executable 'javascript-eslint))
                       (exitcode (and executable (call-process executable nil nil nil
                                                               "--print-config" "asshole.jpg"))))
                  (eq exitcode 0)))
              (setq default-directory (locate-dominating-file default-directory ".eslintrc.js"))
              (flycheck-select-checker 'javascript-eslint)
              (flycheck-mode 1))

            (setq web-mode-code-indent-offset 2
                  web-mode-markup-indent-offset 2

                  tide-always-show-documentation t
                  tide-completion-detailed t))
  :hook ((web-mode . tide-mode)
         (tide-mode . tide-flycheck-setup)
         (tide-mode . prettier-js-mode)
         (tide-mode . tide-hl-identifier-mode))
  :pretty-hydra ((:color "#ff5d38" :quit-key "q" :title "Tide commands")
                 ("Refactor"
                  (("r" tide-refactor               "Refactor")
                   ("y" tide-rename-symbol          "Rename symbol")
                   ("f" tide-rename-symbol          "Rename file")
                   ("i" tide-organize-imports       "Organize imports"))
                  "References"
                  (("a" tide-references             "References")
                   ("d" tide-documentation-at-point "Documentation at point")
                   ("t" tide-nav                    "Find type"))
                  "Errors"
                  (("e" tide-project-errors         "Project errors"))
                  "Misc"
                  (("s" tide-restart-server         "Restart server"))))
  :bind (:map tide-mode-map
              ("C-c t" . tide-hydra/body)))

(use-package toml-mode
  :ensure t
  :commands toml-mode
  :mode ("\\.toml$" . toml-mode))

(use-package yaml-mode
  :ensure t
  :commands yaml-mode
  :mode ("\\.yaml$" . yaml-mode)
  :config (progn
            (add-hook 'yaml-mode-hook 'display-line-numbers-mode))
  :pin melpa-stable)

(use-package org
  :defer t
  :init (use-package suomalainen-kalenteri :ensure t)
  :bind (("C-c a" . org-agenda))
  :config (progn
            ;; org-files
            (setq org-agenda-files '("~/.org" "~/.org/work")
                  org-directory "~/.org")
            ;; store new notes at the top
            (setq org-reverse-note-order t)
            ;; record time when marking an item as `done'
            (setq org-log-done 'time)
            (setq org-log-repeat nil)
            ;; don't show `done' items in the agenda
            (setq org-agenda-skip-scheduled-if-done t)
            (setq org-agenda-skip-deadline-if-done t)
            ;; show all days in agenda, even if there are no items
            (setq org-agenda-show-all-dates t)
            ;; show agenda from current day forward
            (setq org-agenda-start-on-weekday nil)
            ;; no tags in agenda
            (setq org-agenda-remove-tags t)
            ;; dim blocked tasks
            (setq org-agenda-dim-blocked-tasks t)
            ;; compact agenda
            (setq org-agenda-compact-blocks t)
            ;; don't bother with unavailable files
            (setq org-agenda-skip-unavailable-files t)
            ;; show diary/calendar entries
            (setq org-agenda-include-diary t)
            ;; show deadlines 2 weeks prior
            (setq org-deadline-warning-days 14)
            ;; fold by default
            (setq org-startup-folded t)
            ;; indent by default
            (setq org-startup-indented t)
            ;; align tables by default
            (setq org-startup-align-all-tables t)

            ;; nicer styling
            (setq org-fontify-whole-heading-line t
                  org-fontify-done-headline t
                  org-fontify-quote-and-verse-blocks t)

            (setq org-fast-todo-selection t)
            (setq org-todo-keywords
                  '((sequence "TODO(t)" "NEXT(n)" "|" "DONE(d!/@")
                    (sequence "VERIFY(v/!)" "POSTPONED(p@/!)" "|" "CANCELLED(c@|@)")))
            (setq org-todo-state-tags-triggers
                  '(("CANCELLED" ("CANCELLED" . t))
                    ("POSTPONED" ("LATER" . t))
                    ("DONE" ("POSTPONED") ("CANCELLED"))))))

(use-package json-mode
  :ensure t
  :mode ("\\.json$" . json-mode)
  :init (progn
          (add-hook 'json-mode-hook #'set-json-indent-level)
          (add-hook 'json-mode-hook  #'prettier-js-mode))
  :config (progn
            (defun set-json-indent-level ()
              (interactive)
              (setq-local js-indent-level 2)))
  :pin melpa-stable)

(use-package markdown-mode
  :ensure t
  :defer t
  :init (progn
          (add-hook 'markdown-mode-hook #'outline-minor-mode))
  :pin melpa-stable)

(use-package gitconfig-mode
  :ensure t
  :defer t
  :pin melpa-stable)

(use-package gitignore-mode
  :ensure t
  :defer t
  :pin melpa-stable)

(use-package electric
  :ensure t
  :defer t)

(use-package restclient
  :ensure t
  :init (progn
          (use-package company-restclient
            :ensure t
            :init (eval-after-load 'company
                    '(add-to-list 'company-backends 'company-restclient))
            :pin melpa-stable)
          (add-hook 'restclient-response-loaded-hook #'q-to-quit))
  :mode ("\\.rest$" . restclient-mode))

;; load customized variables
(load custom-file)
